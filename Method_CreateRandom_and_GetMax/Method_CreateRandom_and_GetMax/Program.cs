﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Method_CreateRandom_and_GetMax
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] array = CreateRandom(10);

            Print(array);
            var max = GetMax(array);
            Console.WriteLine("\n" + "Max = " + max);


        }

        static int[] CreateRandom(int count, int minvalue = 0, int maxValue = 100)
        {
            var rnd = new Random();
            int[] array = new int[count];
            for (int i = 0; i < array.Length; i++)
            {
                array[i] = rnd.Next(minvalue, maxValue);
            }

            return array;
        }

        static int GetMax(int[] array)
        {
            int max = array[0];
            for (int i = 0; i < array.Length; i++)
            {
                if (array[i] > max)
                {
                    max = array[i];
                }
            }
            return max;
        }

        static void Print(int[] array)
        {
            for (int i = 0; i < array.Length; i++)
            {
                Console.WriteLine("Random number =" + array[i]);
            }
        }
    }
}
